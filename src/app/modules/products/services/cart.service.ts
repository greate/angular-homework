import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class CartService {

  cart = JSON.parse(localStorage.getItem('cart') || '') || [];

  private next() {
    localStorage.setItem(this.storageKey, JSON.stringify(this.cart));
  }

  private storageKey = 'cart';

  constructor() {
    window.addEventListener('storage', (e) => {
      if (e.key === 'cart') {
        let newCart;
        newCart = JSON.parse(e.newValue || '');
        this.cart = newCart;
        this.next();
      }
    });
  }


  addItem(product: any) {
    for (var item in this.cart) {
      if (this.cart[item].name === product.name) {
        if (this.cart[item].count < this.cart[item].available) {
          this.cart[item].count++;
        }
        this.next();
        return;
      }
    }
    this.cart.push({
      name: product.name,
      price: product.price,
      available: product.available,
      count: 1
    });
    this.next();
  }

  addProductCount(product: any) {
    for (var item in this.cart) {
      if (this.cart[item].name === product.name) {
        if (this.cart[item].count < this.cart[item].available) {
          this.cart[item].count++;
        }
      }
    }
    this.next();
  }

  removeProductCount(product: any) {
    for (var item in this.cart) {
      if (this.cart[item].name === product.name) {
        this.cart[item].count--;
        if (this.cart[item].count === 0) {
          this.cart.splice(item, 1);
        }
        break;
      }
    }
    this.next();
  }

  removeItem(product: any) {
    for (var item in this.cart) {
      if (this.cart[item].name === product.name) {
        this.cart.splice(item, 1);
        break;
      }
    }
    this.next();
  }

  productPrice(product: any) {
    var productPrice = 0;
    for (var item in this.cart) {
      if (this.cart[item].name === product.name) {
        productPrice += +(this.cart[item].price * this.cart[item].count).toFixed(2);
      }
    }
    return productPrice;
  }

  totalPrice() {
    var totalCart = 0;
    for (var item in this.cart) {
      totalCart += +(this.cart[item].price * this.cart[item].count).toFixed(2);
    }
    return totalCart;
  }

  getItem(product: any) {
    for (var item in this.cart) {
      if (this.cart[item].name === product.name) {
        return this.cart[item].count;
      }
    }
    return;
  }

  getAllItems() {
    return this.cart;
  }
}
