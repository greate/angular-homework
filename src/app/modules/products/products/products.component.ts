import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Product } from '../shared/product';
import { ProductsApiService } from '../shared/products-api.service';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {

  loading = false;

  constructor(private productsApi: ProductsApiService, private cartService: CartService) { }

  ngOnInit(): void {
    this.loading = true;

    this.productsApi.loadProducts().subscribe(() => {
      this.loading = false;
    });
  }

  addItem(product: any) {
    this.cartService.addItem(product);
  }

  get products() {
    return this.productsApi.products;
  }

  getCartItem(product: any) {
    return this.cartService.getItem(product);
  }
}
