import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from './product';

@Injectable({
  providedIn: 'root'
})
export class ProductsApiService {

  products: Product[] = [];
  productsObj: any = {};
  product: any = {};

  constructor(private http: HttpClient) {
    
  }

  loadProducts() {

    return new Observable(observer => {
      
      if (!this.products.length) {
        this.http.get('/assets/products.json').subscribe(products => {
          this.products = <Product[]>products;
          this.products.forEach(element => {
            this.productsObj[element.id] = element;
          });

          observer.next(products);
          observer.complete();
        });
      } else {
        observer.next(this.products);
        observer.complete();
      }

    });
  }

}
