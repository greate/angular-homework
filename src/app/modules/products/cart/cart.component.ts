import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {

  cart: any = [];

  constructor(private cartService: CartService) { }

  ngOnInit() { }

  addProductCount(product: any) {
    this.cartService.addProductCount(product);
  }

  removeProductCount(product: any) {
      this.cartService.removeProductCount(product);
  }

  removeItem(product: any) {
    this.cartService.removeItem(product);
  }

  get totalPrice() {
    return this.cartService.totalPrice();
  }

  productPrice(product: any) {
    return this.cartService.productPrice(product);
  }

  get products() {
    this.cart = this.cartService.getAllItems();
    return this.cart;
  }
}
