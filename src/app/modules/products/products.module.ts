import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products/products.component';
import { RouterModule, Routes } from '@angular/router';
import { CartComponent } from './cart/cart.component';
import { CartService } from './services/cart.service';

const routes: Routes = [
  {
    path: '', component: ProductsComponent
  }
];

@NgModule({
  declarations: [
    ProductsComponent,
    CartComponent
  ],
  providers: [
    CartService
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ProductsModule { }
